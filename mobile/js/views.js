var ContactsView = Backbone.View.extend({
	/*
	  * @author - Ben Jarman benjarman@me.com
	  * @param contacts - json with the contacts info
	  */
	initialize : function(contacts){
		
		//this line is depreshiated. We no longer load all templates at pageload.
		//this.template = _.template($('#contact-container').html());
		template_loader('contact-list-item', this);
		this.contacts = contacts;
		
		//the call to render has to be made in the ajax call back.
		//this.render();
	},
	render : function(){
		//this.$el.append("<li><a><h3>hello world</h3><p>test</p></a></li>");
		//save the current this and go into a new loop
		contactView = this;
		this.contacts.each(function(contact){
			contactView.$el.append( contactView.template( contact.toJSON() ) );
		});
		return this;
	},
	el : '#contactList',
	
});

var ContactNotesView = Backbone.View.extend({
	el : '#contact-view-content',
	initialize : function(contacts){
		template_loader('notes-list', this);
		this.contact = contacts.first();
		
	},
	render : function(){
		console.log(this.contact);
		this.$el.append( this.template( this.contact.toJSON() ) );
		//this is needed to make jquery mobile play nice
		this.$el.trigger("create");
	}
});
