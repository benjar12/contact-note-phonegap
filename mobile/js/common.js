/**
 * @filenotes :
 * 		-> it is convention here to always use trailing forward slashes
 */



/**
 * @name BASEPATH
 * @type string var
 * @use - base site path
 */
//var BASEPATH = 'http://mobile.contactlist.app/';
var BASEPATH = 'http://' + window.location.host + '/contact-app/';

/**
 * @name TEMPLATEPATH
 * @type string var
 * @use - folder we look for templates in
 */
var TEMPLATEPATH = BASEPATH + 'templates/';


/**
 * @name template_loader
 * @author Ben Jarman
 * @type function
 * @param string - template file name
 * @return html - html from template file
 * @use case - this function will be called to
 * load template files from the templates folder.
 */
var template_loader = function(templateName, view){
	//make a get request
	$.get(TEMPLATEPATH + templateName + '.html', function(html){
		//we should add some kind of fail safe here..
		$('head').append(html);
		//sets the template
		view.template = _.template($('#'+ templateName).html());
		//calls render
		view.render();
	});
	
	
	
};

var get_parameter = function(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};


var load_page_by_name = function(name){
	makeAjaxRequest('contacts.json', name);
};

