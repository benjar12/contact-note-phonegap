var makeAjaxRequest = function(url, callback){
    $.ajax({
		url : url,
		dataType : 'json',
		success : function(json) {
			return ajaxCallBacks[callback](json);
			
		}
	});
    
};

var ajaxCallBacks = {
    /**
    * @type function
    * @author Ben Jarman <benjarman@me.com>
    * @name renderContactList()
    * @param {json} 
    */
    renderContactList : function(json){
    	
    	//we can get rid of the rest of the stuff in the json
    	json = json.Contacts;
    	
    	console.log(json);
    	
    	//init's our collection
    	contacts = new Contacts([]);
    	
    	$.each(json, function(contact){
    		//contact is equal to the id of where it is in
    		//the array not equal to the object.
    		console.log(json[contact]);
    		
    		active = json[contact];
    		
    		//get contact url
    		active['url'] = "./contact-notes.html?id="+contact;
    		
    		//add the object to our collection
    		contacts.add([
    			active
    		]);
    	});
    	
    	//send things off to our view to be rendered
    	contactView = new ContactsView(contacts);
    	
    	
    	//return our collection at the end
    	return contacts;
    	
    },
    renderContactView : function(json){
    	id = get_parameter('id');
    	json = json.Contacts[id];
    	//init's our collection
    	contacts = new Contacts([]);
    	//im pretty sure this isnt best practice
    	//we should be using a seprate model
    	contacts.add([
    		json
    	]);
    	view = new ContactNotesView(contacts);
    	
    	return contacts;
    	
    },
};



